/**
 * Copyright (c) 2013 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "sdk_common.h"
#include "app_error.h"
#if NRF_MODULE_ENABLED(BLE_LBS)
#include "ble_lbs.h"
#include "ble_srv_common.h"
#include <stdlib.h>
#define NRF_LOG_MODULE_NAME ble_lbs
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();


/**@brief Function for handling the Write event.
 *
 * @param[in] p_lbs      LED Button Service structure.
 * @param[in] p_ble_evt  Event received from the BLE stack.
 */
uint8_t gft4_rx_buffer[64];
static void on_write(ble_lbs_t * p_lbs, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    memcpy(gft4_rx_buffer, p_evt_write->data, p_evt_write->len);
    if (   (p_evt_write->handle == p_lbs->led_char_handles.value_handle)
        && (p_evt_write->len >= 1)
        && (p_lbs->led_write_handler != NULL))
    {
        p_lbs->led_write_handler(p_ble_evt->evt.gap_evt.conn_handle, p_lbs, gft4_rx_buffer, p_evt_write->len);
    }
}

bool tx_success = true;
void ble_lbs_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ble_lbs_t * p_lbs = (ble_lbs_t *)p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GATTS_EVT_WRITE:
            on_write(p_lbs, p_ble_evt);
            break;
        case BLE_GATTS_EVT_HVN_TX_COMPLETE:
					  NRF_LOG_INFO("BLE_GATTS_EVT_HVN_TX_COMPLETE ==> 0x%x", BLE_GATTS_EVT_HVN_TX_COMPLETE);
				    tx_success = true;
					  break;
        default:
            // No implementation needed.
            break;
    }
}

#define MAX_PACKET_LENGTH 67
uint32_t ble_lbs_init(ble_lbs_t * p_lbs, const ble_lbs_init_t * p_lbs_init)
{
    uint32_t              err_code;
    ble_uuid_t            ble_uuid;
    ble_add_char_params_t add_char_params;   
	
    // Initialize service structure.
    p_lbs->led_write_handler = p_lbs_init->led_write_handler;                  // Write LED state

    // Add service.
    ble_uuid128_t base_uuid = {LBS_UUID_BASE};
    err_code = sd_ble_uuid_vs_add(&base_uuid, &p_lbs->uuid_type);
		VERIFY_SUCCESS(err_code);		
		NRF_LOG_DEBUG("===>p_lbs->uuid_type = %d", p_lbs->uuid_type);               // Should be equal to "2" ---> Vendor UUID types by Jason    

    ble_uuid.type = p_lbs->uuid_type;
    ble_uuid.uuid = LBS_UUID_SERVICE;

    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_lbs->service_handle);		
    VERIFY_SUCCESS(err_code);		

    // Add Button characteristic.
    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = LBS_UUID_BUTTON_CHAR;
    add_char_params.uuid_type         = p_lbs->uuid_type;
    add_char_params.init_len          = sizeof(uint8_t);
    add_char_params.max_len           = MAX_PACKET_LENGTH;//32;//sizeof(uint8_t);
    add_char_params.char_props.read   = 1;
    add_char_params.char_props.notify = 1;
    add_char_params.is_var_len        = true;                    //   jason
		
    add_char_params.read_access       = SEC_OPEN;
    add_char_params.cccd_write_access = SEC_OPEN;

    err_code = characteristic_add(p_lbs->service_handle,
                                  &add_char_params,
                                  &p_lbs->button_char_handles);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add LED characteristic.
    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid             = LBS_UUID_LED_CHAR;
    add_char_params.uuid_type        = p_lbs->uuid_type;
    add_char_params.init_len         = sizeof(uint8_t);
    add_char_params.max_len          = MAX_PACKET_LENGTH;//sizeof(uint8_t);
    add_char_params.char_props.read  = 1;
    add_char_params.char_props.write = 1;
		add_char_params.is_var_len       = true;                    //   jason

    add_char_params.read_access  = SEC_OPEN;
    add_char_params.write_access = SEC_OPEN;

    return characteristic_add(p_lbs->service_handle, &add_char_params, &p_lbs->led_char_handles);
}

// button state notification to host (smart phone)     Jason
uint16_t len;
uint8_t tx_data[MAX_PACKET_LENGTH];
extern ble_gap_addr_t device_addr;
extern void bsp_board_led_on(uint32_t led_idx);
extern void bsp_board_led_off(uint32_t led_idx);
#define LBS_TIMER_LED                   2                         
uint32_t random_length_packet_send(uint16_t conn_handle, ble_lbs_t * p_lbs, uint8_t button_state)
{	  
	  uint8_t k;
	  //static uint8_t button_status = 0;
	
	  if(button_state)
		{
			bsp_board_led_on(LBS_TIMER_LED);
			//button_status = 0;
		}
		else
		{
			bsp_board_led_off(LBS_TIMER_LED);
			//button_status = 1;
		}
	
	  tx_data[0] = LBS_UUID_ID;//button_status;   //button_state;
	  tx_data[1] = button_state;     //device_addr.addr[0];
		tx_data[2] = device_addr.addr[1];
		tx_data[3] = device_addr.addr[2];
		tx_data[4] = device_addr.addr[3];
		tx_data[5] = device_addr.addr[4];
		tx_data[6] = device_addr.addr[5];				
	
    static ble_gatts_hvx_params_t params;
    len = rand()%MAX_PACKET_LENGTH;//sizeof(button_state);
	  if(len <= 7) 
		{
			len = 7;
		  tx_data[len]++;
		}
		else if(len > 7)
		{
	    for(k = 7; k < len; k++)
		    tx_data[k]++;
		}
    
    memset(&params, 0, sizeof(params));
    params.type   = BLE_GATT_HVX_NOTIFICATION;
    params.handle = p_lbs->button_char_handles.value_handle;
    params.p_data = &tx_data[0];
    params.p_len  = &len;
    
		//NRF_LOG_RED("===>packet length = %d"DEFAULT_COLOR, len); 
    return sd_ble_gatts_hvx(conn_handle, &params);
}

void toggle_status_send(uint16_t conn_handle, ble_lbs_t * p_lbs)
{
	ret_code_t      err_code;  
	
  button_action1 = !button_action1;
	err_code = random_length_packet_send(conn_handle, p_lbs, button_action1);
  if (err_code != NRF_SUCCESS &&
      err_code != BLE_ERROR_INVALID_CONN_HANDLE &&
      err_code != NRF_ERROR_INVALID_STATE &&
      err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
  {
      APP_ERROR_CHECK(err_code);
  }
}
#endif // NRF_MODULE_ENABLED(BLE_LBS)
