/**
 * Copyright (c) 2015 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/**
 * @brief Blinky Sample Application main file.
 *
 * This file contains the source code for a sample server application using the LED Button service.
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_err.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "ble_conn_state.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "boards.h"
#include "app_timer.h"
#include "app_button.h"
#include "ble_lbs.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_ble_lesc.h"
#include "peer_manager.h"
#include "peer_manager_handler.h"
#include "security_manager.h"
#include "security_dispatcher.h"
#include "peer_database.h"
#include "peer_data_storage.h"
#include "id_manager.h"
#include "peer_manager_internal.h"
#include "bsp_btn_ble.h"

#include "fds.h"
#include "fds_app.h"
#include "ble_app.h"

#define NRF_LOG_MODULE_NAME ble_app_c
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_strerror.h"
#include "nrf_log_default_backends.h"
NRF_LOG_MODULE_REGISTER();

#define DEVICE_NAME                       "ArtaFlexGFT4"                       /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME                 "ArtaFlex INC"                       /**< Manufacturer. Will be passed to Device Information Service. */        // 2019.11.12

#define APP_BLE_OBSERVER_PRIO           3                                       /**< Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_CONN_CFG_TAG            1                                       /**< A tag identifying the SoftDevice BLE configuration. */

#define APP_ADV_INTERVAL                64  /* 64 = 64 * 0.625ms = 40ms */      /**< The advertising interval (in units of 0.625 ms; this value corresponds to 40 ms). */
#define APP_ADV_DURATION                BLE_GAP_ADV_TIMEOUT_GENERAL_UNLIMITED   /**< The advertising time-out (in units of seconds). When set to 0, we will never time out. */


#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(100, UNIT_1_25_MS)        /**< Minimum acceptable connection interval (0.5 seconds). */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(200, UNIT_1_25_MS)        /**< Maximum acceptable connection interval (1 second). */
#define SLAVE_LATENCY                   0                                       /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Connection supervisory time-out (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(20000)                  /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (15 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000)                   /**< Time between each call to sd_ble_gap_conn_param_update after the first call (5 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                       /**< Number of attempts before giving up the connection parameter negotiation. */

#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50)                     /**< Delay from a GPIOTE event until a button is reported as pushed (in number of timer ticks). */

#define SEC_PARAM_BOND                      1                                       /**< Perform bonding. */
#define SEC_PARAM_MITM                      0                                       /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES           BLE_GAP_IO_CAPS_NONE                    /**< No I/O capabilities. */
#define SEC_PARAM_LESC                      1                                       /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS                  0                                       /**< Keypress notifications not enabled. */
#define SEC_PARAM_OOB                       0                                       /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE              7                                       /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE              16                                      /**< Maximum encryption key size. */

#define DEAD_BEEF                       0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

BLE_LBS_DEF(m_lbs);                                                             /**< LED Button Service instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                       /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                                         /**< Context for the Queued Write module.*/

APP_TIMER_DEF(m_lbs_timer_id);                                                  /**< Heart rate measurement timer. */
#define LBS_TIMER_INTERVAL            APP_TIMER_TICKS(1000)                     /**< Heart rate measurement interval (ticks). */

uint8_t connect_adv = 0;
uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;                               /**< Handle of the current connection. */
volatile uint8_t ble_connection = 0;

static uint8_t m_adv_handle = BLE_GAP_ADV_SET_HANDLE_NOT_SET;                   /**< Advertising handle used to identify an advertising set. */
static uint8_t m_enc_advdata[BLE_GAP_ADV_SET_DATA_SIZE_MAX];                    /**< Buffer for storing an encoded advertising set. */
static uint8_t m_enc_scan_response_data[BLE_GAP_ADV_SET_DATA_SIZE_MAX];         /**< Buffer for storing an encoded scan data. */

static ble_gap_addr_t remote_addr;
static ret_code_t pm_peer_get_addr(ble_gap_addr_t *remote_addr);
uint8_t gft_role;
/**@brief Struct that contains pointers to the encoded advertising data. */
#define RSSI_CHANGED_REPORT_ENABLE  0
#define LONG_RANGE_SELECTED         1

static char * roles_str[] =
{
    "Invalid Role",
    "Peripheral",
    "Central",	
};

#if LONG_RANGE_SELECTED
static ble_gap_adv_data_t m_adv_data =
{
    .adv_data =
    {
        .p_data = m_enc_advdata,
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX
    },
    .scan_rsp_data =
    {
        .p_data = m_enc_scan_response_data,
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX

    }
};

typedef enum
{
    SELECTION_0_dBm = 0, 
    SELECTION_8_dBm = 8
} output_power_seclection_t;

// Settings like ATT MTU size are set only once, on the dummy board.
// Make sure that defaults are sensible.
test_params_t m_test_params =
{	
    .erase_at_startup = false,
    .timer_enable     = false,			
	  .phy_select       = 2,
   // Only symmetric PHYs are supported.	
    .phys.tx_phys     = BLE_GAP_PHY_CODED,
    .phys.rx_phys     = BLE_GAP_PHY_CODED,		
		.mode             = WORKING_MODE,
};

char const * phy_str(ble_gap_phys_t phys)
{
    static char const * str[] =
    {
        "1 Mbps",
        "2 Mbps",
        "Coded",
        "Unknown"
    };

    switch (phys.tx_phys)
    {
        case BLE_GAP_PHY_1MBPS:
            return str[0];

        case BLE_GAP_PHY_2MBPS:
        case BLE_GAP_PHY_2MBPS | BLE_GAP_PHY_1MBPS:
        case BLE_GAP_PHY_2MBPS | BLE_GAP_PHY_1MBPS | BLE_GAP_PHY_CODED:
            return str[1];

        case BLE_GAP_PHY_CODED:
            return str[2];

        default:
            return str[3];
    }
}

// Type holding the two output power options for this application.
#else
static ble_gap_adv_data_t m_adv_data =
{
    .adv_data =
    {
        .p_data = m_enc_advdata,
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX
    },
    .scan_rsp_data =
    {
        .p_data = m_enc_scan_response_data,
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX

    }
};
#endif

/**@brief Function for handling the Heart rate measurement timer timeout.
 *
 * @details This function will be called each time the heart rate measurement timer expires.
 *          It will exclude RR Interval data from every third measurement.
 *
 * @param[in] p_context  Pointer used for passing some arbitrary information (context) from the
 *                       app_start_timer() call to the timeout handler.
 */
ret_code_t lbs_data_send(void)
{
    ret_code_t      err_code;
  	button_action1 = !button_action1;
	  tx_success = false;
	  err_code = random_length_packet_send(m_conn_handle, &m_lbs, button_action1);
    if (err_code != NRF_SUCCESS &&
        err_code != BLE_ERROR_INVALID_CONN_HANDLE &&
        err_code != NRF_ERROR_INVALID_STATE &&
        err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
       {
         //APP_ERROR_CHECK(err_code);
       }
    return err_code;			 
}

uint8_t  button_action1;
static void lbs_timer_timeout_handler(void * p_context)                         // Added by Jason
{
  UNUSED_PARAMETER(p_context);
	(void)lbs_data_send();
}

void app_timer_enable(bool timer_enable)
{
  ret_code_t      err_code;
	
	if(timer_enable)//m_test_params.timer_enable)
	   err_code = app_timer_start(m_lbs_timer_id, LBS_TIMER_INTERVAL, NULL);	
	else
		 err_code = app_timer_stop(m_lbs_timer_id);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */

//void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
//{
//    app_error_handler(DEAD_BEEF, line_num, p_file_name);
//}


/**@brief Function for the LEDs initialization.
 *
 * @details Initializes all LEDs used by the application.
 */
void leds_init(void)
{
    bsp_board_init(BSP_INIT_LEDS);
}


/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module.
 */
void timers_init(void)
{
    // Initialize timer module, making it use the scheduler
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_lbs_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                lbs_timer_timeout_handler);
    APP_ERROR_CHECK(err_code);
}

/**@brief Clear bond information from persistent storage.
 */
static void delete_bonds(void)
{
    ret_code_t err_code;

    NRF_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for starting advertising.
 */
void advertising_start(bool erase_bonds)
{	
    if (erase_bonds == true)
    {        
        // Advertising is started by PM_EVT_PEERS_DELETE_SUCCEEDED event.
#if MYFLASH_FDS_ENABLE
		//delete_all_begin();
			delete_bonds();
#else
			delete_bonds();
#endif			
    }
    else
    {
        ret_code_t err_code;	
			
		#if LONG_RANGE_SELECTED
				err_code = sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV, m_adv_handle, SELECTION_8_dBm);
				APP_ERROR_CHECK(err_code);
		#endif
				err_code = sd_ble_gap_adv_start(m_adv_handle, APP_BLE_CONN_CFG_TAG);
				APP_ERROR_CHECK(err_code);
			
			  bsp_board_led_on(ADVERTISING_LED);
    }
}

/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
//  ret_code_t err_code;
	
    pm_handler_on_pm_evt(p_evt);
    pm_handler_flash_clean(p_evt);

    switch (p_evt->evt_id)
    {
        case PM_EVT_PEERS_DELETE_SUCCEEDED:
					NRF_LOG_DEBUG("Erase bonds ====> Peers Delete Succeeded!");
					//advertising_start(false);
				  //NVIC_SystemReset();
					break;
				case PM_EVT_CONN_SEC_FAILED:
				{					
            gft_role = ble_conn_state_role(p_evt->conn_handle);
            NRF_LOG_BLUE("Role %s:a call to PM_peer_delete() peer id %d, Line: %d"DEFAULT_COLOR, (gft_role == BLE_GAP_ROLE_CENTRAL)?"Periph":"Central", p_evt->peer_id, __LINE__);									
          //m_test_params.erase_at_startup = true;
#if MYFLASH_FDS_ENABLE
					if(m_test_params.mode == BONDING_MODE)
						Bonded_delete_begin();
          //delete_all_begin();
          //delete_bonds();                       
			    //NVIC_SystemReset();		
#endif					
					ble_connection = 2;
				}break;
        case PM_EVT_CONN_SEC_SUCCEEDED:
#if 0					
          NRF_LOG_RED("PM %s ==> Sending PHY Update request, %s, Line: %d"DEFAULT_COLOR, 
				                     roles_str[ble_conn_state_role(p_evt->conn_handle)], 
				                     phy_str(m_test_params.phys), 
				                     __LINE__);
          err_code = sd_ble_gap_phy_update(m_conn_handle, &m_test_params.phys);
          APP_ERROR_CHECK(err_code);							
#endif				
				  ble_connection = 1;
      		break;			
        default:
          break;				
    }
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
ble_gap_addr_t device_addr;
uint8_t device_full_name[12 + 5 + 1];
const uint8_t dec_To_hex[] = "0123456789ABCDEF";
static ble_gap_conn_params_t   gap_conn_params;
void gap_params_init(void)
{
    ret_code_t              err_code;
  //ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_addr_get(&device_addr);
	  APP_ERROR_CHECK(err_code);
	  NRF_LOG_DEBUG(" local Device Address: Line: %d", __LINE__);
	  NRF_LOG_HEXDUMP_DEBUG(device_addr.addr, BLE_GAP_ADDR_LEN);
	  memcpy(device_full_name, (const uint8_t *)DEVICE_NAME, strlen(DEVICE_NAME));
	  device_full_name[12]= dec_To_hex[(device_addr.addr[3] >> 0) & 0x0F];
	  device_full_name[13]= dec_To_hex[(device_addr.addr[4] >> 4) & 0x0F];
	  device_full_name[14]= dec_To_hex[(device_addr.addr[4] >> 0) & 0x0F];
	  device_full_name[15]= dec_To_hex[(device_addr.addr[5] >> 4) & 0x0F];
	  device_full_name[16]= dec_To_hex[(device_addr.addr[5] >> 0) & 0x0F];
	  device_full_name[17]= 0x00;
	  NRF_LOG_DEBUG("Device Name: %s",  device_full_name);
	
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                        //(const uint8_t *)DEVICE_NAME,
                                        //strlen(DEVICE_NAME));
                                          (const uint8_t *)device_full_name,
                                          12 + 4);																					
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the GATT module.
 */
void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for the Peer Manager initialization.
 */
void peer_manager_init(void)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Advertising functionality.
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 */
void advertising_init(void)
{
    ret_code_t    err_code;
    ble_advdata_t advdata;
    ble_advdata_t srdata;

    ble_uuid_t adv_uuids[] = {{LBS_UUID_SERVICE, m_lbs.uuid_type}};

    // Build and set advertising data.
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type          = BLE_ADVDATA_FULL_NAME;//BLE_ADVDATA_SHORT_NAME;//BLE_ADVDATA_FULL_NAME;
  //advdata.short_name_len     = 9;
    advdata.include_appearance = true;
    advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    //advdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    //advdata.uuids_complete.p_uuids  = adv_uuids;

    memset(&srdata, 0, sizeof(srdata));
    srdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    srdata.uuids_complete.p_uuids  = adv_uuids;
    
    err_code = ble_advdata_encode(&advdata, m_adv_data.adv_data.p_data, &m_adv_data.adv_data.len);
    APP_ERROR_CHECK(err_code);

    err_code = ble_advdata_encode(&srdata, m_adv_data.scan_rsp_data.p_data, &m_adv_data.scan_rsp_data.len);				
    APP_ERROR_CHECK(err_code);		

    ble_gap_adv_params_t adv_params;

    // Set advertising parameters.
    memset(&adv_params, 0, sizeof(adv_params));

#if LONG_RANGE_SELECTED
    NRF_LOG_DEBUG("Advertising type set to BLE_GAP_ADV_TYPE_CONNECTABLE_SCANNABLE_UNDIRECTED ");
    adv_params.properties.type = BLE_GAP_ADV_TYPE_CONNECTABLE_SCANNABLE_UNDIRECTED;//BLE_GAP_ADV_TYPE_EXTENDED_CONNECTABLE_NONSCANNABLE_UNDIRECTED;
    adv_params.p_peer_addr   = NULL;
    adv_params.filter_policy = BLE_GAP_ADV_FP_ANY;
    adv_params.interval      = APP_ADV_INTERVAL;
    adv_params.duration      = 0;//APP_ADV_DURATION;
    NRF_LOG_DEBUG("Setting adv params phy to %s .. ", "PHY 1MBps");//phy_str(m_test_params.phys));
    adv_params.primary_phy     = BLE_GAP_PHY_1MBPS;//BLE_GAP_PHY_CODED;
    adv_params.secondary_phy   = BLE_GAP_PHY_1MBPS;//BLE_GAP_PHY_CODED;
    adv_params.scan_req_notification =1; //recently added MD 7/9/18		
#else
    adv_params.primary_phy     = BLE_GAP_PHY_1MBPS;
    adv_params.duration        = APP_ADV_DURATION;
    adv_params.properties.type = BLE_GAP_ADV_TYPE_CONNECTABLE_SCANNABLE_UNDIRECTED;
    adv_params.p_peer_addr     = NULL;
    adv_params.filter_policy   = BLE_GAP_ADV_FP_ANY;
    adv_params.interval        = APP_ADV_INTERVAL;
#endif
    err_code = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_adv_data, &adv_params);		
    APP_ERROR_CHECK(err_code);
		NRF_LOG_DEBUG("===>adv handle generated : 0x%x", m_adv_handle);
}


/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for handling write events to the LED characteristic.
 *
 * @param[in] p_lbs     Instance of LED Button Service to which the write applies.
 * @param[in] led_state Written/desired state of the LED.
 */
static void led_write_handler(uint16_t conn_handle, ble_lbs_t * p_lbs, uint8_t *data, uint8_t length)
{
	  static bool led_toggle;
//  ret_code_t    err_code;

    led_toggle = !led_toggle;

		NRF_LOG_RED("==>len = %d", length);
		NRF_LOG_HEXDUMP_INFO(data, length);
		NRF_LOG_INFO(DEFAULT_COLOR"             "DEFAULT_COLOR);	
    if (led_toggle)
    {
        bsp_board_led_on(LEDBUTTON_LED);
        NRF_LOG_INFO("Received LED ON!");
    }
    else
    {
        bsp_board_led_off(LEDBUTTON_LED);
        NRF_LOG_INFO("Received LED OFF!");
    }
		
	  if (data[1] == SYSTEM_RESET)
    {
			NVIC_SystemReset();
    }
    else if (data[1] == ERASE_IN_START)
    {			
			m_test_params.erase_at_startup = true;
#if MYFLASH_FDS_ENABLE			
			update_fds_begin();			
			//NVIC_SystemReset();
#else
			advertising_start(m_test_params.erase_bonds);
#endif			
			NRF_LOG_INFO("ERASE_IN_START!");
    }		
		else if (data[1] == DELETE_ALL_CONFIG)
		{
			//delete_all_begin();
		}
		else if (data[1] == TIMER_ENABLE)
		{
			m_test_params.timer_enable = !m_test_params.timer_enable;
#if 0			
#if MYFLASH_FDS_ENABLE			
			update_fds_begin();
#endif	
  		if(m_test_params.timer_enable)
		    err_code = app_timer_start(m_lbs_timer_id, LBS_TIMER_INTERVAL, NULL);	
			else
				err_code = app_timer_stop(m_lbs_timer_id);
      APP_ERROR_CHECK(err_code);
#endif			
		}
		else if (data[1] == GFT4_DISCONNECT)
		{
#if 0			
      err_code = pm_peer_get_addr(&remote_addr1);
      APP_ERROR_CHECK(err_code);
			
      err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
      APP_ERROR_CHECK(err_code);		

    //nrf_mydelay_ms(2000);
			uint32_t peer_count = pm_peer_count();
			if(peer_count > 0)
			{							 							
    #if 1	
			  conn_params_init();
        NRF_LOG_DEBUG(DEFAULT_COLOR"Connecting.......by Peer Address======>");                         // Jason					
        err_code = sd_ble_gap_connect(&remote_addr1,
                                      &m_scan_param,
                                      &gap_conn_params,
                                      APP_BLE_CONN_CFG_TAG);	
        APP_ERROR_CHECK(err_code);
    #else
        advertising_start(false);
    #endif
      }
#endif			
		}
		else if (data[1] == GFT4_CONNECT)
		{
			
		}
		else if (data[1] == GFT_MODE)
		{
			if(m_test_params.mode == WORKING_MODE)
				m_test_params.mode = BONDING_MODE;
			else if(m_test_params.mode == BONDING_MODE)
				m_test_params.mode = WORKING_MODE;
#if MYFLASH_FDS_ENABLE			
			update_fds_begin();
#endif			
		}
}


/**@brief Function for initializing services that will be used by the application.
 */
void services_init(void)
{
    ret_code_t         err_code;
    ble_lbs_init_t     init     = {0};
    nrf_ble_qwr_init_t qwr_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

    // Initialize LBS.
    init.led_write_handler = led_write_handler;

    err_code = ble_lbs_init(&m_lbs, &init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module that
 *          are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply
 *       setting the disconnect_on_fail config parameter, but instead we use the event
 *       handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;
	
    memset(&cp_init, 0, sizeof(cp_init));
	
	  err_code = ble_conn_params_stop();
	  APP_ERROR_CHECK(err_code);

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;                           // m_lbs.button_char_handles.cccd_handle
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}

#if RSSI_CHANGED_REPORT_ENABLE
/**@brief Function for enabling reception of RSSI values when in a connection. 
 */
static void rssi_measurements_start(uint16_t m_conn_handle)
{
  uint8_t threshold    = 2;
  uint8_t skip_count   = 10;
  ret_code_t err_code = sd_ble_gap_rssi_start(m_conn_handle, threshold, skip_count);
  APP_ERROR_CHECK(err_code);

}
#endif

/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code;
//	  m_gap_role           = p_ble_evt->evt.gap_evt.params.connected.role;
	  ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;
	
    static int8_t rssi_value = 0;
    uint8_t       channel_rssi;		
	
	  char          passkey[BLE_GAP_PASSKEY_LEN + 1];
	  gft_role = ble_conn_state_role(p_ble_evt->evt.gap_evt.conn_handle);	

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            //NRF_LOG_INFO("Connected");
            connect_adv = 0;
            bsp_board_led_on(CONNECTED_LED);
            bsp_board_led_off(ADVERTISING_LED);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);
          //err_code = app_button_enable();
          //APP_ERROR_CHECK(err_code);
				    
				    NRF_LOG_PURPLE("As %s Connected in m_conn_handle: 0x%x"DEFAULT_COLOR, nrf_log_push(roles_str[gft_role]), m_conn_handle);
				
				    err_code = sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_CONN, m_conn_handle, SELECTION_8_dBm);
				    APP_ERROR_CHECK(err_code);
				
#if RSSI_CHANGED_REPORT_ENABLE				
            rssi_measurements_start(p_gap_evt->conn_handle);				
#endif				
#if 0				
				    if (role == BLE_GAP_ROLE_PERIPH)
            {
              NRF_LOG_RED("Periphera ==> Sending PHY Update request, %s."DEFAULT_COLOR, phy_str(m_test_params.phys));
            }
						else if (role == BLE_GAP_ROLE_CENTRAL)
						{
							NRF_LOG_RED("Central   ==> Sending PHY Update request, %s."DEFAULT_COLOR, phy_str(m_test_params.phys));
						}
            err_code = sd_ble_gap_phy_update(m_conn_handle, &m_test_params.phys);
            APP_ERROR_CHECK(err_code);
#endif				        
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_RED("As %s Disconnected"DEFAULT_COLOR, nrf_log_push(roles_str[gft_role]));
            connect_adv = 1;
            bsp_board_led_off(CONNECTED_LED);
            m_conn_handle = BLE_CONN_HANDLE_INVALID;				
				    err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);				                
            APP_ERROR_CHECK(err_code);				    
          //err_code = app_button_disable();
          //APP_ERROR_CHECK(err_code);
          //err_code = ble_conn_params_stop();
          //advertising_start(false);
          //update_fds_pending_clear();
				    ble_connection = 0;
            break;
				
        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            NRF_LOG_BLUE("%s: BLE_GAP_EVT_SEC_PARAMS_REQUEST"DEFAULT_COLOR, nrf_log_push(roles_str[gft_role])); //BLE_GAP_ROLE_PERIPH]));
            //err_code = sd_ble_gap_sec_params_reply(m_conn_handle,
            //                                       BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP,
            //                                       NULL,
            //                                       NULL);
            //APP_ERROR_CHECK(err_code);				
            break;
				
        case BLE_GAP_EVT_PASSKEY_DISPLAY:
            memcpy(passkey, p_ble_evt->evt.gap_evt.params.passkey_display.passkey, BLE_GAP_PASSKEY_LEN);
            passkey[BLE_GAP_PASSKEY_LEN] = 0x00;
            NRF_LOG_BLUE("%s: BLE_GAP_EVT_PASSKEY_DISPLAY: passkey=%s match_req=%d"DEFAULT_COLOR,
                         nrf_log_push(roles_str[gft_role]),                                                     //BLE_GAP_ROLE_PERIPH]),
                         nrf_log_push(passkey),
                         p_ble_evt->evt.gap_evt.params.passkey_display.match_request);

            if (p_ble_evt->evt.gap_evt.params.passkey_display.match_request)
            {
                //on_match_request(conn_handle, role);
            }
            break;
						
        case BLE_GAP_EVT_AUTH_KEY_REQUEST:
            NRF_LOG_BLUE("%s: BLE_GAP_EVT_AUTH_KEY_REQUEST"DEFAULT_COLOR, nrf_log_push(roles_str[gft_role]));  //BLE_GAP_ROLE_PERIPH]));
            break;

        case BLE_GAP_EVT_LESC_DHKEY_REQUEST:
            NRF_LOG_BLUE("%s: BLE_GAP_EVT_LESC_DHKEY_REQUEST"DEFAULT_COLOR, nrf_log_push(roles_str[gft_role]));//BLE_GAP_ROLE_PERIPH]));
            break;

         case BLE_GAP_EVT_AUTH_STATUS:
             NRF_LOG_INFO("%s: BLE_GAP_EVT_AUTH_STATUS: status=0x%x bond=0x%x lv4: %d kdist_own:0x%x kdist_peer:0x%x"DEFAULT_COLOR,
                          nrf_log_push(roles_str[gft_role]),                                                   //BLE_GAP_ROLE_PERIPH]),
                          p_ble_evt->evt.gap_evt.params.auth_status.auth_status,
                          p_ble_evt->evt.gap_evt.params.auth_status.bonded,
                          p_ble_evt->evt.gap_evt.params.auth_status.sm1_levels.lv4,
                          *((uint8_t *)&p_ble_evt->evt.gap_evt.params.auth_status.kdist_own),
                          *((uint8_t *)&p_ble_evt->evt.gap_evt.params.auth_status.kdist_peer));				 
            break;
				 
        case BLE_GAP_EVT_PHY_UPDATE:
        {
            ble_gap_evt_phy_update_t const * p_phy_evt = &p_ble_evt->evt.gap_evt.params.phy_update;

            if (p_phy_evt->status == BLE_HCI_STATUS_CODE_LMP_ERROR_TRANSACTION_COLLISION)
            {
                // Ignore LL collisions.
                NRF_LOG_DEBUG("LL transaction collision during PHY update.");
                break;
            }

            ble_gap_phys_t phys = {0};
            phys.tx_phys = p_phy_evt->tx_phy;
            phys.rx_phys = p_phy_evt->rx_phy;
            NRF_LOG_RED("%s PHY updated as %s, PHY set %s, Line: %d"DEFAULT_COLOR, nrf_log_push(roles_str[gft_role]),
						                                                                phy_str(phys), 
						                                                               (p_phy_evt->status == BLE_HCI_STATUS_CODE_SUCCESS) ? "accepted" : "rejected",
						                                                                __LINE__);
        } break;					

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
					ble_gap_evt_phy_update_t const * p_phy_evt = &p_ble_evt->evt.gap_evt.params.phy_update;					         
          ble_gap_phys_t const phys =
          {
              .rx_phys = p_phy_evt->rx_phy,
              .tx_phys = p_phy_evt->tx_phy,
          };
					NRF_LOG_RED("PHY update request: %s, Line: %d"DEFAULT_COLOR, phy_str(phys), __LINE__);
          err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
					if(err_code != NRF_SUCCESS)
						NRF_LOG_RED("PHY update fails!....., Line: %d", __LINE__);
          //APP_ERROR_CHECK(err_code);									
        } break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
				    NRF_LOG_DEBUG("BLE_GATTS_EVT_SYS_ATTR_MISSING....");
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;
        
        case BLE_GAP_EVT_RSSI_CHANGED:
            rssi_value =  p_gap_evt->params.rssi_changed.rssi;
            channel_rssi =  p_gap_evt->params.rssi_changed.ch_index;
            NRF_LOG_INFO("RSSI changed, new: %d, channel: %d",rssi_value, channel_rssi); 
				    break;				 
				 
        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}

#define IM_MAX_CONN_HANDLES             (20)                      // See Line 66 of "id_manager.c" file
static ret_code_t pm_peer_get_addr(ble_gap_addr_t *remote_addr)
{
    uint16_t   conn_handle;
    ret_code_t err_code;

    pm_peer_id_t current_peer_id = pds_next_peer_id_get(PM_PEER_ID_INVALID);

    while (current_peer_id != PM_PEER_ID_INVALID)
    {
        conn_handle = im_conn_handle_get(current_peer_id);

        if (conn_handle < IM_MAX_CONN_HANDLES)
        {
					 err_code = im_ble_addr_get(conn_handle, remote_addr);
					 APP_ERROR_CHECK(err_code);
					 break;
        }
				
        current_peer_id = pds_next_peer_id_get(current_peer_id);
    }

    return NRF_SUCCESS;
}


/**@brief Function for handling events from the button handler module.
 *
 * @param[in] pin_no        The pin that the event applies to.
 * @param[in] button_action The button action (press/release).
 */

// <o> NRF_BLE_SCAN_SCAN_INTERVAL - Scanning interval. Determines the scan interval in units of 0.625 millisecond. 
#ifndef NRF_BLE_SCAN_SCAN_INTERVAL
#define NRF_BLE_SCAN_SCAN_INTERVAL 160
#endif

// <o> NRF_BLE_SCAN_SCAN_DURATION - Duration of a scanning session in units of 10 ms. Range: 0x0001 - 0xFFFF (10 ms to 10.9225 ms). If set to 0x0000, the scanning continues until it is explicitly disabled. 
#ifndef NRF_BLE_SCAN_SCAN_DURATION
#define NRF_BLE_SCAN_SCAN_DURATION 0
#endif

// <o> NRF_BLE_SCAN_SCAN_WINDOW - Scanning window. Determines the scanning window in units of 0.625 millisecond. 
#ifndef NRF_BLE_SCAN_SCAN_WINDOW
#define NRF_BLE_SCAN_SCAN_WINDOW 80
#endif

static ble_gap_scan_params_t m_scan_param =                 /**< Scan parameters requested for scanning and connection. */
{
    .active        = 0x01,
    .interval      = NRF_BLE_SCAN_SCAN_INTERVAL,
    .window        = NRF_BLE_SCAN_SCAN_WINDOW,
    .filter_policy = BLE_GAP_SCAN_FP_ACCEPT_ALL,
    .timeout       = NRF_BLE_SCAN_SCAN_DURATION,
    .scan_phys     = BLE_GAP_PHY_1MBPS,
    .extended      = true,                                 // Changed by Jason Chen from false to true
};

static void button_event_handler(uint8_t pin_no, uint8_t button_action)
{
    ret_code_t err_code;	  
	
    switch (pin_no)
    {
        case LEDBUTTON_BUTTON:                            // button 1
            //NRF_LOG_INFO("Send button state change.");
				    if(!button_action) break;
#if 1				    
				    (void)lbs_data_send();
#else				
				    button_action1 = !button_action1;
            err_code = random_length_packet_send(m_conn_handle, &m_lbs, button_action1);
            if (err_code != NRF_SUCCESS &&
                err_code != BLE_ERROR_INVALID_CONN_HANDLE &&
                err_code != NRF_ERROR_INVALID_STATE &&
                err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
            {
                APP_ERROR_CHECK(err_code);
            }
#endif						
	          //if(button_action)
			      //  bsp_board_led_on(LBS_TIMER_LED);
		        //else
			      //  bsp_board_led_off(LBS_TIMER_LED);						
            break;
				case LBS_DELETE_BUTTON:                                    // button 2
					if(button_action)
					{
						m_test_params.phy_select++;
						if(m_test_params.phy_select > 2)
							m_test_params.phy_select = 0;
						
						if(m_test_params.phy_select == 0)
						{
							m_test_params.phys.rx_phys = BLE_GAP_PHY_1MBPS;
							m_test_params.phys.tx_phys = BLE_GAP_PHY_1MBPS;
							NRF_LOG_BLUE("Connecting.......by BLE_GAP_PHY_1MBPS======>"DEFAULT_COLOR);														
						}
						else if(m_test_params.phy_select == 1)
						{
							m_test_params.phys.rx_phys = BLE_GAP_PHY_2MBPS | BLE_GAP_PHY_1MBPS | BLE_GAP_PHY_CODED;
							m_test_params.phys.tx_phys = BLE_GAP_PHY_2MBPS | BLE_GAP_PHY_1MBPS | BLE_GAP_PHY_CODED;
							NRF_LOG_BLUE("Connecting.......by BLE_GAP_PHY_2MBPS======>"DEFAULT_COLOR);							
						}
						else if(m_test_params.phy_select == 2)
						{
							m_test_params.phys.rx_phys = BLE_GAP_PHY_CODED;
							m_test_params.phys.tx_phys = BLE_GAP_PHY_CODED;
							NRF_LOG_BLUE("Connecting.......by BLE_GAP_Coded======>"DEFAULT_COLOR);													
						}					
						err_code = sd_ble_gap_phy_update(m_conn_handle, &m_test_params.phys);
						APP_ERROR_CHECK(err_code);

#if MYFLASH_FDS_ENABLE
            update_fds_begin();
#endif
					}
					break;

				case LBS_DISCONNECT_BUTTON:                        // Button 3
#if 1
          if(button_action)
          {
             if(connect_adv == 0)
             {
               NRF_LOG_BLUE(" Get Remote Device Address, Line: %d", __LINE__);
               err_code = pm_peer_get_addr(&remote_addr);
               APP_ERROR_CHECK(err_code);	             

               err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);//BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);//BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
						   if(err_code == NRF_ERROR_INVALID_STATE)
						   {
							   NRF_LOG_DEBUG("===>Disconnection in progress or link has not been established...");
						   }
						   else if(err_code == NRF_SUCCESS)
						   {
							   NRF_LOG_DEBUG("===>The disconnection procedure has been started successfully....");
						   }
						   else
						   {
							   NRF_LOG_DEBUG("===>sd_ble_gap_disconnect() err_code = 0x%x", err_code);
						   }													 
							 //connect_adv = 1;
						 }
						 else if(connect_adv == 1)
						 {
        #if 1							 
						   uint32_t peer_count = pm_peer_count();
						   if(peer_count > 0)
						   {							 							
                  //ble_gap_addr_t remote_addr;
                  //err_code = pm_peer_get_addr(&remote_addr);
							    //APP_ERROR_CHECK(err_code);
	                NRF_LOG_BLUE(" Remote Device Address:");								 
	                NRF_LOG_HEXDUMP_DEBUG(remote_addr.addr, BLE_GAP_ADDR_LEN);													

							  //conn_params_init();
                  NRF_LOG_DEBUG(DEFAULT_COLOR"Connecting.......by Peer Address======>");                         // Jason					
						      err_code = sd_ble_gap_connect(&remote_addr,
						                                    &m_scan_param,
						                                   &gap_conn_params,
						                                   APP_BLE_CONN_CFG_TAG);	
							    APP_ERROR_CHECK(err_code);
               }														
				#endif		
							 //advertising_start(false);
							 //connect_adv = 0;
						 }
				  }
#endif
					break;
					
        case LBS_TIMER_BUTTON:                                      // Button 4
            if(button_action)
            {
              m_test_params.timer_enable = !m_test_params.timer_enable;
#if MYFLASH_FDS_ENABLE			
			        //update_fds_begin();
#endif					
#if 0
              if(m_test_params.timer_enable)
              {
                bsp_board_led_on(LBS_TIMER_LED);
                err_code = app_timer_start(m_lbs_timer_id, LBS_TIMER_INTERVAL, NULL);
                APP_ERROR_CHECK(err_code);
                NRF_LOG_INFO("Start Timer........");
              }
              else
              {
                err_code = app_timer_stop(m_lbs_timer_id);
                APP_ERROR_CHECK(err_code);
                bsp_board_led_off(LBS_TIMER_LED);
                NRF_LOG_INFO("Stop Timer........");
              }
#endif							
            }
            break;
        default:
            APP_ERROR_HANDLER(pin_no);
            break;
    }
}

ret_code_t re_connect(uint32_t line)
{
	ret_code_t err_code;
	
  NRF_LOG_GREEN("Re_Connectto addr, Line: %d"DEFAULT_COLOR, line)
	NRF_LOG_HEXDUMP_INFO(remote_addr.addr, BLE_GAP_ADDR_LEN);
	if( (remote_addr.addr[0] == 0x00)||(remote_addr.addr[1] == 0x00)||(remote_addr.addr[2] == 0x00)
		||(remote_addr.addr[3] == 0x00)||(remote_addr.addr[4] == 0x00)||(remote_addr.addr[5] == 0x00))
		return NRF_ERROR_INVALID_DATA; 
	err_code = sd_ble_gap_connect(&remote_addr,    &m_scan_param,
	                              &gap_conn_params, APP_BLE_CONN_CFG_TAG);	
//APP_ERROR_CHECK(err_code);
  return err_code;
}

/**@brief Function for initializing the button handler module.
 */
void buttons_init(bool * p_erase_bonds)
{
    ret_code_t err_code;
	  bsp_event_t startup_event;

    //The array must be static because a pointer to it will be saved in the button handler module.
    static app_button_cfg_t buttons[] =
    {
        {LEDBUTTON_BUTTON,      false, BUTTON_PULL, button_event_handler},
				{LBS_TIMER_BUTTON,      false, BUTTON_PULL, button_event_handler},
				{LBS_DISCONNECT_BUTTON, false, BUTTON_PULL, button_event_handler},
				{LBS_DELETE_BUTTON,     false, BUTTON_PULL, button_event_handler}				
    };

    err_code = app_button_init(buttons, ARRAY_SIZE(buttons),
                               BUTTON_DETECTION_DELAY);
		APP_ERROR_CHECK(err_code);

		 err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);

    *p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
		
    err_code = app_button_enable();
    APP_ERROR_CHECK(err_code);    
}


void log_init(void)
{
    //ret_code_t err_code = NRF_LOG_INIT(NULL);
	  ret_code_t err_code = NRF_LOG_INIT(app_timer_cnt_get);
		
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing power management.
 */
void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
void idle_state_handle(void)
{
#if PM_LESC_ENABLED
    ret_code_t err_code;	
    err_code = nrf_ble_lesc_request_handler();
    APP_ERROR_CHECK(err_code);
#endif
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}

void get_peer_ble_addr(uint32_t line)
{
	ret_code_t           err_code;
	//uint8_t const       *peer_addr;
	pm_peer_data_flash_t peer_data;
	
	memset(remote_addr.addr, 0, BLE_GAP_ADDR_LEN); 
  remote_addr.addr_type    = 0;
	remote_addr.addr_id_peer = 0;	
  
  pm_peer_id_t current_peer_id = pds_next_peer_id_get(PM_PEER_ID_INVALID);

  while ((current_peer_id != PM_PEER_ID_INVALID)&&(current_peer_id < PM_PEER_ID_N_AVAILABLE_IDS))
  {
     err_code = pdb_peer_data_ptr_get(current_peer_id, PM_PEER_DATA_ID_BONDING, &peer_data);	

     if (err_code == NRF_SUCCESS)
     {					
			 NRF_LOG_GREEN("add peer_id = %d, Line: %d"DEFAULT_COLOR, current_peer_id, line)
			 NRF_LOG_HEXDUMP_INFO(peer_data.p_bonding_data->peer_ble_id.id_addr_info.addr, BLE_GAP_ADDR_LEN);

       //memcpy(remote_addr.addr, peer_data.p_bonding_data->peer_ble_id.id_addr_info.addr,BLE_GAP_ADDR_LEN);
	  	 //remote_addr.addr_type    = peer_data.p_bonding_data->peer_ble_id.id_addr_info.addr_type;
			 //remote_addr.addr_id_peer = peer_data.p_bonding_data->peer_ble_id.id_addr_info.addr_id_peer;
			 
			 remote_addr =  peer_data.p_bonding_data->peer_ble_id.id_addr_info;
			 return;
      }   				
      current_peer_id = pds_next_peer_id_get(current_peer_id);
  }
  NRF_LOG_GREEN("add peer_id = %d, Line: %d"DEFAULT_COLOR, current_peer_id, line)
	NRF_LOG_HEXDUMP_INFO(peer_data.p_bonding_data->peer_ble_id.id_addr_info.addr, BLE_GAP_ADDR_LEN);	
}



/**
 * @}
 */
