/**
 * Copyright (c) 2015 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/**
 * @brief Blinky Sample Application main file.
 *
 * This file contains the source code for a sample server application using the LED Button service.
 */
#ifndef __BLE_APP_H__
#define __BLE_APP_H__

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "ble_gap.h"
#include "fds.h"
#include "nrf_delay.h"

#define ADVERTISING_LED                 BSP_BOARD_LED_0                         /**< Is on when device is advertising. */
#define CONNECTED_LED                   BSP_BOARD_LED_1                         /**< Is on when device has connected. */
#define LEDBUTTON_LED                   BSP_BOARD_LED_2                         /**< LED to be toggled with the help of the LED Button Service. */
#define LBS_TIMER_LED                   BSP_BOARD_LED_3                         
#define LEDBUTTON_BUTTON                BSP_BUTTON_0                            /**< Button that will trigger the notification event with the LED Button Service */
#define LBS_DELETE_BUTTON               BSP_BUTTON_1                            /**< Button that will trigger Bond delete*/
#define LBS_DISCONNECT_BUTTON           BSP_BUTTON_2                            /**< Button that will trigger Bond delete*/
#define LBS_TIMER_BUTTON                BSP_BUTTON_3                            /**< Button that will trigger the timer notification*/

typedef enum
{
	SYSTEM_RESET        = 0x01,
  ERASE_IN_START      = 0x02, 
	DELETE_ALL_CONFIG   = 0x03,
	TIMER_ENABLE        = 0x04,
	GFT4_DISCONNECT     = 0x05,
	GFT4_CONNECT        = 0x06,
	GFT_MODE            = 0x71,
	ERASE_BONDED        = 0x72,
} gft4_rx_command_t;

extern uint16_t m_conn_handle;
extern volatile uint8_t ble_connection;
ret_code_t lbs_data_send(void);
extern uint8_t gft_role;
char const * phy_str(ble_gap_phys_t phys);

typedef struct
{
    uint8_t        phy_select;	
    ble_gap_phys_t phys;                       /**< Preferred PHYs. */
	  bool           erase_at_startup;
    bool           timer_enable;
	  uint8_t        mode;
} test_params_t;

extern test_params_t m_test_params;
extern uint8_t device_full_name[12 + 5 + 1];

void log_init(void);
void leds_init(void);
void timers_init(void);
void buttons_init(bool * p_erase_bonds);
void power_management_init(void);
void ble_stack_init(void);
void gap_params_init(void);
void gatt_init(void);
void services_init(void);
void advertising_init(void);
void conn_params_init(void);
void peer_manager_init(void);
void advertising_start(bool erase_bonds);
void idle_state_handle(void);

void app_timer_enable(bool timer_enable);
void get_peer_ble_addr(uint32_t line);
ret_code_t re_connect(uint32_t line);

__STATIC_INLINE void nrf_mydelay_ms(uint32_t ms_time)
{
    if (ms_time == 0)
    {
        return;
    }

    do {
        nrf_delay_us(1000);
			  idle_state_handle();
    } while (--ms_time);
}

#endif

/**
 * @}
 */
