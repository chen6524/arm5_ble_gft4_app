/**
 * Copyright (c) 2015 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/**
 * @brief Blinky Sample Application main file.
 *
 * This file contains the source code for a sample server application using the LED Button service.
 */

#include <stdint.h>
#include <string.h>
#include "fds.h"
#include "ble_app.h"
#include "app_timer.h"

#include "fds_app.h"
#include "rtc_internal.h"
#include "timer.h"
#include "ble_lbs.h"
#include "boards.h"
#include "nrf_delay.h"

#define NRF_LOG_MODULE_NAME main_c
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_strerror.h"
#include "nrf_log_default_backends.h"
NRF_LOG_MODULE_REGISTER();

/**@brief Function for application main entry.
 */
volatile static uint32_t second_timer;
static void timer_conn_tx_dic(void);
int main(void)
{
	  bool erase_bonds;
	
    // Initialize.
    log_init();
    leds_init();
    timers_init();
    buttons_init(&erase_bonds);
    
	  power_management_init();
    ble_stack_init();
    gap_params_init();
	
    gatt_init();
    services_init();
    advertising_init();
    conn_params_init();
	
	  peer_manager_init();	

		rtc_internal_init();
		accurate_timer_init();
			
#if MYFLASH_FDS_ENABLE
    fds_app_init();
#endif		
    // Start execution.
    NRF_LOG_INFO("GFT4 evl board Example Started, Erase: %s, size: %d", m_test_params.erase_at_startup?"TRUE":"FALSE", sizeof(configuration_t));
		NRF_LOG_INFO("                         Timer Status: %s,", m_test_params.timer_enable?"Enable":"Disable");

    advertising_start(m_test_params.erase_at_startup);    //||erase_bonds);
    if(m_test_params.erase_at_startup)
		{
		  m_test_params.erase_at_startup = false;
#if MYFLASH_FDS_ENABLE			
	  //gft4_config_update();
      update_fds_begin();
#endif	
		}
		if (erase_bonds == true)
    {        
      delete_all_begin();
    }
#if 1
  	if(m_test_params.timer_enable)
		{
			//app_timer_enable(m_test_params.timer_enable);
		}
#endif	
    get_peer_ble_addr(__LINE__);
    // Enter main loop. 
    for (;;)
    {
#if MYFLASH_FDS_ENABLE
			fds_process();
		//fds_manage();
#endif			
      idle_state_handle();			
      timer_conn_tx_dic();
    }
}

static uint8_t tx_dic_stm = 0;
static uint8_t conn_fail_cnt = 0;
static void timer_conn_tx_dic(void)
{
	ret_code_t err_code;
	static uint32_t dealy_count = 0;
	static uint32_t conn_count = 0;
	static uint16_t radom_dealy_time;
	
	if(second_timer != timer_1_ms)			
	{
	  second_timer = timer_1_ms;
    if(m_test_params.timer_enable)
  	{			
			dealy_count++;
			switch(tx_dic_stm)
			{
				case 0:
					if(ble_connection == 0)
					{
						dealy_count = 0;
						//NRF_LOG_DEBUG("===>reconnection==========================>dealy_count = %d, Line:%d", dealy_count, __LINE__);
						bsp_board_led_invert(BSP_BOARD_LED_2);
					//advertising_start(false);
						err_code = re_connect(__LINE__);
						if(err_code != NRF_SUCCESS)
						{
							ble_connection = 0xFE;
							tx_dic_stm = 1;
							break;
						}
						NRF_LOG_DEBUG("===>reconnection==========================>dealy_count = %d, Line:%d", dealy_count, __LINE__);
						tx_dic_stm = 1;
						ble_connection = 0xFF;
					}
					else if(ble_connection == 1)
					{
						if(gft_role == BLE_GAP_ROLE_CENTRAL)
						{
							tx_dic_stm = 1;
						}
						else if(gft_role == BLE_GAP_ROLE_PERIPH)
						{
							tx_dic_stm = 3;
						}
					}
					break;
				case 1:
					//NRF_LOG_BLUE("===>waiting connect..... ==========================>...%d, Line:%d"DEFAULT_COLOR, tx_success, __LINE__);
          if(ble_connection == 1)
					{
						conn_count = 0;
						NRF_LOG_DEBUG("===>Reconnection OK=========================>dealy_count = %d, Line:%d", dealy_count, __LINE__);
            err_code = lbs_data_send();
						if(err_code == NRF_SUCCESS)
						{
							tx_dic_stm = 2;
						}
						else
						{
						  NRF_LOG_BLUE("===>data tx fails      ==========================>...%d, Line:%d"DEFAULT_COLOR, tx_success, __LINE__);
							tx_dic_stm = 0;
						}						
					}
					else if(ble_connection == 2)
					{
						conn_count = 0;
						conn_fail_cnt++;
						NRF_LOG_BLUE("===>Connection fails, fail count: %d"DEFAULT_COLOR, conn_fail_cnt);
						if(conn_fail_cnt > 5)
						{
							//Bonded_delete_begin();
							ble_connection = 0;
							tx_dic_stm = 0;
						}
					}
					else if(ble_connection == 0xFE)
					{
            conn_count++;
						radom_dealy_time = rand()%10;
						if(radom_dealy_time < 3)
						{
							radom_dealy_time = 3;
							radom_dealy_time = radom_dealy_time*100;
						}
						else
						{
							radom_dealy_time = radom_dealy_time *100;
						}
						if(conn_count > radom_dealy_time)
						{
							conn_count = 0;
							tx_dic_stm = 0;
							ble_connection = 0;							
						}
					//sizeof(button_state);
						//NRF_LOG_BLUE("===>Connection fails, fail count: %d, Line: %d"DEFAULT_COLOR, conn_fail_cnt, __LINE__);
					}
					else
					{
						conn_count++;
						if(conn_count > 4000)
						{
							NRF_LOG_BLUE("===>Connection fails, re_connect: %d, Line: %d"DEFAULT_COLOR, conn_count, __LINE__);
							(void)sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
							conn_count = 0;
							tx_dic_stm = 0;
							ble_connection = 0;
						}
					}
					break;
				case 2:
          if(tx_success)
		      {
						NRF_LOG_BLUE ("===>data tx in success ==========================================>dealy_count = %4d, %d"DEFAULT_COLOR, dealy_count, __LINE__);
				  //NRF_LOG_DEBUG("===>The disconnection procedure has been started successfully====>dealy_count = %d, %d\r\n", dealy_count, __LINE__);
						tx_dic_stm = 5;
          }					
          break;
				case 3:
					if(ble_connection == 1)
					{
						NRF_LOG_GREEN("===>Call disconnect...........dealy_count: %d"DEFAULT_COLOR, dealy_count);
						err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
						if(err_code == NRF_SUCCESS)
						{
							tx_dic_stm = 4;
						}						
						else if(err_code == NRF_ERROR_INVALID_STATE)
						{
							NRF_LOG_RED("===>Disconnection in progress or link has not been established..."DEFAULT_COLOR);
						}
						else
						{
							NRF_LOG_RED("===>sd_ble_gap_disconnect() err_code = 0x%x"DEFAULT_COLOR, err_code);
						}
					}
					else if((ble_connection == 0)||(ble_connection == 2))
					{
						ble_connection = 0;
						tx_dic_stm = 0;
					}
          break;
				case 4:
					if(ble_connection == 0)
					{
						tx_dic_stm = 5;
						NRF_LOG_DEBUG("===>The disconnection procedure has been started successfully....,dealy_count = %d", dealy_count);
					}
          else if(ble_connection == 2)
					{						
						tx_dic_stm = 5;
					}
          break;
				case 5:
					if(dealy_count > 4999)                 // delay 4*125 = 500 ms
					{
					  NRF_LOG_GREEN("===>The disconnection procedure has been started successfully====>dealy_count = %d, %d\r\n"DEFAULT_COLOR, dealy_count, __LINE__);
						tx_dic_stm = 0;
						dealy_count = 0;
					}
					break;
				default:
					tx_dic_stm = 0;				  
					break;
			}
		}
	}
}

static void timer_conn_tx_dic1(void)
{
	ret_code_t err_code;
	static uint32_t dealy_count = 0;
	static uint32_t conn_count = 0;
	static uint16_t radom_dealy_time;
	
	if(second_timer != timer_1_ms)			
	{
	  second_timer = timer_1_ms;
    if(m_test_params.timer_enable)
  	{			
			dealy_count++;
			switch(tx_dic_stm)
			{
				case 0:
					if(ble_connection == 0)
					{
						dealy_count = 0;
						//NRF_LOG_DEBUG("===>reconnection==========================>dealy_count = %d, Line:%d", dealy_count, __LINE__);
						bsp_board_led_invert(BSP_BOARD_LED_2);
					//advertising_start(false);
						err_code = re_connect(__LINE__);
						if(err_code != NRF_SUCCESS)
						{
							ble_connection = 0xFE;
							tx_dic_stm = 1;
							break;
						}
						NRF_LOG_DEBUG("===>reconnection==========================>dealy_count = %d, Line:%d", dealy_count, __LINE__);
						tx_dic_stm = 1;
						ble_connection = 0xFF;
					}
					else if(ble_connection == 1)
					{
						tx_dic_stm = 3;
					}
					break;
				case 1:
					//NRF_LOG_BLUE("===>waiting connect..... ==========================>...%d, Line:%d"DEFAULT_COLOR, tx_success, __LINE__);
          if(ble_connection == 1)
					{
						conn_count = 0;
						NRF_LOG_DEBUG("===>Reconnection OK=========================>dealy_count = %d, Line:%d", dealy_count, __LINE__);
            err_code = lbs_data_send();
						if(err_code == NRF_SUCCESS)
						{
							tx_dic_stm = 2;
						}
						else
						{
						  NRF_LOG_BLUE("===>data tx fails      ==========================>...%d, Line:%d"DEFAULT_COLOR, tx_success, __LINE__);
							tx_dic_stm = 0;
						}						
					}
					else if(ble_connection == 2)
					{
						conn_count = 0;
						conn_fail_cnt++;
						NRF_LOG_BLUE("===>Connection fails, fail count: %d"DEFAULT_COLOR, conn_fail_cnt);
						if(conn_fail_cnt > 5)
						{
							//Bonded_delete_begin();
							ble_connection = 0;
							tx_dic_stm = 0;
						}
					}
					else if(ble_connection == 0xFE)
					{
            conn_count++;
						radom_dealy_time = rand()%10;
						if(radom_dealy_time < 3)
						{
							radom_dealy_time = 3;
							radom_dealy_time = radom_dealy_time*100;
						}
						else
						{
							radom_dealy_time = radom_dealy_time *100;
						}
						if(conn_count > radom_dealy_time)
						{
							conn_count = 0;
							tx_dic_stm = 0;
							ble_connection = 0;							
						}
					//sizeof(button_state);
						//NRF_LOG_BLUE("===>Connection fails, fail count: %d, Line: %d"DEFAULT_COLOR, conn_fail_cnt, __LINE__);
					}
					else
					{
						conn_count++;
						if(conn_count > 4000)
						{
							NRF_LOG_BLUE("===>Connection fails, re_connect: %d, Line: %d"DEFAULT_COLOR, conn_count, __LINE__);
							(void)sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
							conn_count = 0;
							tx_dic_stm = 0;
							ble_connection = 0;
						}
					}
					break;
				case 2:
          if(tx_success)
		      {
						NRF_LOG_BLUE("===>data tx in success ==========================>dealy_count = %d, Line:%d"DEFAULT_COLOR, dealy_count, __LINE__);
						tx_dic_stm = 3;
          }					
          break;
				case 3:
					if(ble_connection == 1)
					{
						NRF_LOG_GREEN("===>Call disconnect...........dealy_count: %d"DEFAULT_COLOR, dealy_count);
						err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
						if(err_code == NRF_SUCCESS)
						{
							tx_dic_stm = 4;
						}						
						else if(err_code == NRF_ERROR_INVALID_STATE)
						{
							NRF_LOG_RED("===>Disconnection in progress or link has not been established..."DEFAULT_COLOR);
						}
						else
						{
							NRF_LOG_RED("===>sd_ble_gap_disconnect() err_code = 0x%x"DEFAULT_COLOR, err_code);
						}
					}
					else if((ble_connection == 0)||(ble_connection == 2))
					{
						ble_connection = 0;
						tx_dic_stm = 0;
					}
          break;
				case 4:
					if(ble_connection == 0)
					{
						tx_dic_stm = 5;
						NRF_LOG_DEBUG("===>The disconnection procedure has been started successfully....,dealy_count = %d", dealy_count);
					}
          else if(ble_connection == 2)
					{						
						tx_dic_stm = 5;
					}
          break;
				case 5:
					if(dealy_count > 4999)                 // delay 4*125 = 500 ms
					{
						NRF_LOG_DEBUG("===>The disconnection procedure has been started successfully....,dealy_count = %d, %d\r\n", dealy_count, __LINE__);
						tx_dic_stm = 0;
						dealy_count = 0;
					}
					break;
				default:
					tx_dic_stm = 0;				  
					break;
			}
		}
	}
}
/**
 * @}
 */
