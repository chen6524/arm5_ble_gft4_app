/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0148
 * Fax:   (905) 479-0149
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#ifndef _RTC_INTERNAL_H_
#define _RTC_INTERNAL_H_

//#include <stdint.h>
//#include <stdbool.h>
#include "nrf_drv_rtc.h"

extern uint32_t rtc_second;

void rtc_internal_handler(nrf_drv_rtc_int_type_t int_type);
void rtc_internal_init(void);
#endif
